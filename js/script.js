/**
 * scripts.js
 * Contains Script for basic static website named "White Graphics"
 */

/********************************************************************************
                                PRELOADER
********************************************************************************/

$(window).on('load', function () {
    $('#preloader').delay(500).fadeOut('slow');
});

/********************************************************************************
                                OWL CAROUSEL
********************************************************************************/

$(document).ready(function () {
    $("#team-right").owlCarousel({
        items: 2,
        autoplay: true,
        margin: 20,
        loop: true,
        smartSpeed: 700, //transition time
        autoplayHoverPause: true,
        nav: true,
        dots: false,
        navText: ['<i class="lni-chevron-left-circle"></i>', '<i class="lni-chevron-right-circle"></i>']
    });
});

/********************************************************************************
                                WAYPOINT
********************************************************************************/

$(document).ready(function () {
    $("#progress-elements").waypoint(function () {
        $(".progress-bar").each(function () {

            $(this).animate({
                width: $(this).attr("aria-valuenow") + "%"
            }, 800);
        });
        this.destroy;
    }, {
        offset: 'bottom-in-view'
    });
});

/********************************************************************************
                                RESPONSIVE TABS
********************************************************************************/

$(document).ready(function () {
    $('#services-tabs').responsiveTabs({
        startCollapsed: 'accordion',
        animation: 'slide',
    });
});

/********************************************************************************
                                PORTFOLIO SECTION
********************************************************************************/

$(document).ready(function () {
    $('#isotope-filters').on("click", "button", function () {
        //on(event, child, function) 

        let filterValue = $(this).attr("data-filter");
        $('#isotope-container').isotope({
            filter: filterValue
        });

        //active button
        $("#isotope-filters").find('.active').removeClass('active');
        $(this).addClass('active');

    });
});

$(document).ready(function () {
    $('#portfolio-wrapper').magnificPopup({
        delegate: 'a',
        type: 'image',
        gallery: {
            enabled: true
        },
        zoom: {
            enabled: true,
            duration: 300,
            easing: 'ease-in-out',
            opener: function (openerElement) {
                return openerElement.is('img') ? openerElement : openerElement.find('img');
            }
        }
    });
});

/********************************************************************************
                                TESTIMONIAL SECTION
********************************************************************************/

$(function () {
    $('#testimonial-slider').owlCarousel({
        items: 1,
        autoplay: true,
        smartSpeed: 700,
        autoplayHoverPause: true,
        loop: true,
        nav: true,
        dots: false,
        navText: ['<i class="lni-chevron-left-circle"></i>', '<i class="lni-chevron-right-circle"></i>']
    });
});

/********************************************************************************
                                STATS SECTION
********************************************************************************/

$(function () {
    $('.counter').counterUp({
        delay: 10,
        time: 1000
    });
});


/********************************************************************************
                                CLIENTS SECTION
********************************************************************************/

$(function () {
    $('#client-list').owlCarousel({
        items: 6,
        autoplay: true,
        smartSpeed: 700,
        autoplayHoverPause: true,
        loop: true,
        nav: true,
        dots: false,
        navText: ['<i class="lni-chevron-left-circle"></i>', '<i class="lni-chevron-right-circle"></i>'],
        responsive: {
            0: {
                items: 2
            },
            480: {
                items: 3
            },
            768: {
                items: 6
            }
        }
    });
});

/********************************************************************************
                                MAP SECTION
********************************************************************************/

$(window).on('load', function () {
    var addressString = "301, Evergreen CHS., Airoli, Maharashtra, India"
    var myLatLng = {
        lat: 19.174022,
        lng: 72.953705
    }

    var myMap = new google.maps.Map(document.getElementById('map'), {
        zoom: 17,
        center: myLatLng
    });

    var marker = new google.maps.Marker({
        position: myLatLng,
        map: myMap,
        title: 'Click to see Address'
    });

    var infoWindow = new google.maps.infoWindow({
        content: addressString
    });

    marker.addListener('click', function () {
        infoWindow.open(myMap, marker);
    })
});

/********************************************************************************
                                NAVIGATION
********************************************************************************/


$(function () {
//    $(document).ready(function()) ka short version
    showHideNav();
    $(window).scroll(function(){
        showHideNav();
    });
    
    function showHideNav(){
        if($(window).scrollTop() > 50){
            $("nav").addClass("scrolled-navbar green-nav-top");
            $(".navbar-brand img").attr('src', "img/logo/logo-dark.png");
            $("#back-to-top").fadeIn();
        }else{
            $("nav").removeClass("scrolled-navbar green-nav-top");
            $(".navbar-brand img").attr('src', "img/logo/logo.png");
            $("#back-to-top").fadeOut();
        }
    }
});


/********************************************************************************
                                MOBILE NAV
********************************************************************************/

$(function(){
    
   $("#mobile-nav-open-btn").click(function(){
       $("#mobile-nav").css("height", "100%");
   });
    
    $("#mobile-nav-close-btn, #mobile-nav a").click(function(){
       $("#mobile-nav").css("height", "0%");
   });
});

/********************************************************************************
                                SMOOTH SCROLL
********************************************************************************/

$(function(){
   $("a.smooth-scroll").click(function(event){
       event.preventDefault();
       var section_id = $(this).attr("href");
       $("html, body").animate({
           scrollTop: $(section_id).offset().top -50
       }, 1250, "easeInOutExpo");
   });
});
























